package ru.neoflex.practice.Contoller;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@OpenAPIDefinition(
        info = @Info(
                title = "Клиент-серверное приложение калькулятора",
                version = "1.0",
                description = "API для калькулятора"
        )
)


@RestController
@SpringBootApplication
public class CalcContoller {

    @GetMapping("/plus/{a}/{b}")
    public static int amount(@PathVariable int a, @PathVariable int b){
        return a + b;
    }

    @GetMapping("/minus/{a}/{b}")
    public static int subtraction(@PathVariable int a, @PathVariable int b){
        return a - b;
    }
}